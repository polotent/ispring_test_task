from os import path
import app.main.db as db_ops
from .helpers import parse_rates, get_currencies_info, get_datamart_query
from .app import app
from .etl import fetch_currency_rates
from config import Config


@app.task
def etl():
    # Get currency rates from remote source (latest info).
    rates = fetch_currency_rates(Config.CURR_RATES_SOURCE_URL,
                                 Config.CURR_RATES_SOURCE_ACCESS_KEY,
                                 Config.CURR_BASE,
                                 Config.CURR_LIST)

    # Create (if not exists) and update Staging section for saving latest rates.
    db_ops.create_stagingrates_table()
    db_ops.update_stagingrates_table(parse_rates(rates))
    return "etl task completed"


@app.task
def update_core():
    # Check for Staging section. If not exists: return, because no source available for
    # updating core.
    if not db_ops.check_table_exists("StagingRates"):
        return "update_core task completed, no staging table was found, so core wasn't updated"
    # Create Core Section for transfering data from Staging section.
    if not db_ops.check_table_exists("Currencies"):
        db_ops.create_currencies_table()
        # Seed currencies table with data from json file.
        basedir = path.abspath(path.dirname(__file__))
        currencies_info = get_currencies_info(path.join(basedir, "db", "currency_multilang_info.json"))
        db_ops.seed_currencies_table(currencies_info)

    db_ops.create_corerates_table()

    # Update Core section (transfer data from Staging section).
    db_ops.update_corerates_table()
    return "update_core task completed"


@app.task
def generate_datamarts():
    # Check for Core section. If not exists: return, because no source available for
    # creating datamarts section.
    if not db_ops.check_table_exists("CoreRates"):
        return "generate_datamarts task completed, no datamarts were generated, because no core was found"

    # Get datamarts configuration info from json config file.
    basedir = path.abspath(path.dirname(__file__))
    datamarts_sql_queries_path = path.join(basedir, "db", "datamarts_sql_queries")
    datamarts_config_path = path.join(basedir, "datamarts_config.json")

    # Generate datamarts section.
    db_ops.create_datamart_table(get_datamart_query(1, datamarts_sql_queries_path, datamarts_config_path))
    db_ops.create_datamart_table(get_datamart_query(2, datamarts_sql_queries_path, datamarts_config_path))
    db_ops.create_datamart_table(get_datamart_query(3, datamarts_sql_queries_path, datamarts_config_path))
    return "generate_datamarts task completed"
