import requests


def fetch_currency_rates(url, access_key, curr_base, curr_list):
    params = {
        "access_key": access_key,
        "base": curr_base,
        "symbols": curr_list
    }
    try:
        result = requests.get(f"{url}", params=params).json()
        if "error" not in result and "success" in result:
            if result["success"]:
                return result
        return None
    except requests.exceptions.RequestException:
        return None
