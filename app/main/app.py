from celery import Celery
from .db.misc_operations import create_celery_logs_db, create_dwh_db, use_dwh_db


# Create dwh db if not exists and use it with cursor in mysql.connect
create_dwh_db()
use_dwh_db()

# Celery instance.
app = Celery()
app.config_from_object('app.main.celeryconfig')
