import json
from os import path
from config import Config
from datetime import datetime


def parse_rates(rates):
    base_mark = rates["base"]
    dt = datetime.fromtimestamp(rates["timestamp"])
    rates_dict = rates["rates"]
    values_str = ""
    for key, value in rates_dict.items():
        values_str += f"(\"{dt}\", \"{base_mark}\", \"{key}\", {value}), "
    values_str = values_str[:-2]
    return values_str


def get_currencies_info(filepath):
    try:
        info_obj = None
        with open(filepath, "r") as f:
            info_obj = json.loads(f.read())
        currencies_info = ""
        for mark, lang_obj in info_obj.items():
            currencies_info += f"""("{mark}", "{lang_obj['english']}",
                                    "{lang_obj['deutsch']}", "{lang_obj['russian']}",
                                    "{lang_obj['chinese']}"), """
        currencies_info = currencies_info[:-2]
    except Exception:
        return None
    return currencies_info


def get_datamart_query(n, datamarts_sql_queries_path, datamarts_config_path):
    try:
        with open(path.join(datamarts_sql_queries_path, f"datamart{n}.sql"), "r") as f:
            datamart_query = f.read()
        with open(datamarts_config_path, "r") as f:
            common_config_obj = json.loads(f.read())
        if f"datamart{n}" in common_config_obj:
            datamart_config = common_config_obj[f"datamart{n}"]
        else:
            return None
        for key, value in datamart_config.items():
            datamart_query = datamart_query.replace(f"${key}$", value)
        datamart_query = datamart_query.replace("$agent_mark$", Config.CURR_BASE)
    except Exception:
        return None
    return datamart_query.split(';')
