import mysql.connector
from config import Config


db = mysql.connector.connect(
    host=Config.DWH_DB_HOST,
    user=Config.DWH_DB_USER,
    passwd=Config.DWH_DB_PASSWORD
)

dwh_cursor = db.cursor(buffered=True)
