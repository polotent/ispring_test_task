import mysql.connector
from .connect import db, dwh_cursor


def create_corerates_table():
    try:
        dwh_cursor.execute("""CREATE TABLE IF NOT EXISTS CoreRates
            (id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
            datetime datetime NOT NULL,
            base_currency_id int NOT NULL,
            target_currency_id int NOT NULL,
            rate float(40,20) NOT NULL,
            CONSTRAINT rate_info UNIQUE(datetime,base_currency_id,target_currency_id),
            FOREIGN KEY (base_currency_id) REFERENCES Currencies(id),
            FOREIGN KEY (target_currency_id) REFERENCES Currencies(id));""")
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None


def update_corerates_table():
    try:
        dwh_cursor.execute("""
            INSERT INTO CoreRates (
                CoreRates.datetime,
                CoreRates.base_currency_id,
                CoreRates.target_currency_id,
                CoreRates.rate
            )
            SELECT StagingRates.datetime,
                (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = StagingRates.base_mark),
                (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = StagingRates.target_mark),
                StagingRates.rate
            FROM StagingRates
            WHERE StagingRates.datetime > IFNULL((SELECT MAX(CoreRates.datetime) FROM CoreRates), CAST(0 AS DATE));""")
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None


def create_currencies_table():
    try:
        dwh_cursor.execute("""CREATE TABLE IF NOT EXISTS Currencies
            (id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
            mark varchar(3) NOT NULL,
            english_name varchar(50) NOT NULL,
            deutsch_name varchar(50) NOT NULL,
            russian_name varchar(50) NOT NULL,
            chinese_name varchar(50) NOT NULL);""")
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None


def seed_currencies_table(values):
    try:
        dwh_cursor.execute(f"""
        INSERT INTO Currencies (
            Currencies.mark,
            Currencies.english_name,
            Currencies.deutsch_name,
            Currencies.russian_name,
            Currencies.chinese_name
        ) VALUES {values};""")
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None
