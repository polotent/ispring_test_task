import mysql.connector
from .connect import db, dwh_cursor


def create_datamart_table(queries):
    try:
        for query in queries:
            if query.strip() != '':
                dwh_cursor.execute(query)
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None
