import mysql.connector
from .connect import db, dwh_cursor


def create_stagingrates_table():
    try:
        dwh_cursor.execute("""CREATE TABLE IF NOT EXISTS StagingRates
            (id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
            datetime datetime NOT NULL,
            base_mark varchar(3) NOT NULL,
            target_mark varchar(3) NOT NULL,
            rate float(40,20) NOT NULL,
            CONSTRAINT rate_info UNIQUE(datetime,base_mark,target_mark));""")
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None


def update_stagingrates_table(values):
    insert_query = f"INSERT INTO StagingRates (datetime, base_mark, target_mark, rate) VALUES {values};"
    try:
        dwh_cursor.execute(insert_query)
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None
