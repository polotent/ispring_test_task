import mysql.connector
from .connect import db, dwh_cursor


def create_dwh_db():
    try:
        dwh_cursor.execute("""CREATE DATABASE IF NOT EXISTS dwh;""")
        db.commit()
    except mysql.connector.Error:
        db.rollback()
    return None


def use_dwh_db():
    try:
        dwh_cursor.execute("""USE dwh;""")
    except mysql.connector.Error:
        pass
    return None


def check_table_exists(tablename):
    dwh_cursor.execute(f"""SHOW TABLES LIKE \"{tablename}\";""")
    if dwh_cursor.fetchone():
        return True
    return False
