DROP TABLE IF EXISTS f1;
CREATE TABLE f1 (
    datetime datetime NOT NULL,
    base_mark varchar(3) NOT NULL,
    target_mark varchar(3) NOT NULL,
    rate float(40,20) NOT NULL
);

INSERT INTO f1 (f1.datetime, f1.base_mark, f1.target_mark, f1.rate)
SELECT CoreRates.datetime, "$base_mark$", "$agent_mark$", 1 / CoreRates.rate
FROM CoreRates
WHERE "$start_datetime$" <= datetime 
    AND datetime <= "$end_datetime$"
    AND base_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$agent_mark$")
    AND target_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$base_mark$");

DROP TABLE IF EXISTS f2;
CREATE TABLE f2 (
    datetime datetime NOT NULL,
    base_mark varchar(3) NOT NULL,
    target_mark varchar(3) NOT NULL,
    rate float(40,20) NOT NULL
);

INSERT INTO f2 (f2.datetime, f2.base_mark, f2.target_mark, f2.rate)
SELECT CoreRates.datetime, "$agent_mark$", "$target_mark$", CoreRates.rate
FROM CoreRates
WHERE "$start_datetime$" <= datetime 
    AND datetime <= "$end_datetime$"
    AND base_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$agent_mark$")
    AND target_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$target_mark$");

DROP TABLE IF EXISTS DataMart3;
CREATE TABLE DataMart3
            (datetime datetime NOT NULL,
            base_mark varchar(3) NOT NULL,
            target_mark varchar(3) NOT NULL,
            rate float(40,20) NOT NULL);
INSERT INTO DataMart3 (
    DataMart3.datetime,
    DataMart3.base_mark,
    DataMart3.target_mark,
    DataMart3.rate) 
SELECT f1.datetime, f1.base_mark, f2.target_mark, f1.rate * f2.rate AS rate
FROM f1
LEFT JOIN f2
ON f1.datetime = f2.datetime;

DROP TABLE f1;
DROP TABLE f2;  
