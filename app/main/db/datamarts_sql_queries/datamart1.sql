DROP TABLE IF EXISTS f1;
CREATE TABLE f1 (
    datetime datetime NOT NULL,
    base_currency_name varchar(50) NOT NULL,
    target_currency_name varchar(50) NOT NULL,
    rate float(40,20) NOT NULL
);

INSERT INTO f1 (f1.datetime, f1.base_currency_name, f1.target_currency_name, f1.rate)
SELECT 
    CoreRates.datetime, 
    (SELECT $language$_name FROM Currencies WHERE Currencies.id=CoreRates.target_currency_id), 
    (SELECT $language$_name FROM Currencies WHERE Currencies.id=CoreRates.base_currency_id), 
    1 / CoreRates.rate
FROM CoreRates
WHERE datetime = (SELECT MAX(datetime) FROM CoreRates)
    AND base_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$agent_mark$");

DROP TABLE IF EXISTS f2;
CREATE TABLE f2 (
    datetime datetime NOT NULL,
    base_currency_name varchar(50) NOT NULL,
    target_currency_name varchar(50) NOT NULL,
    rate float(40,20) NOT NULL
);

INSERT INTO f2 (f2.datetime, f2.base_currency_name, f2.target_currency_name, f2.rate)
SELECT
    CoreRates.datetime, 
    (SELECT $language$_name FROM Currencies WHERE Currencies.id=CoreRates.base_currency_id), 
    (SELECT $language$_name FROM Currencies WHERE Currencies.id=CoreRates.target_currency_id),
    CoreRates.rate
FROM CoreRates
WHERE datetime = (SELECT MAX(datetime) FROM CoreRates)
    AND base_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$agent_mark$")
    AND target_currency_id = (SELECT Currencies.id FROM Currencies WHERE Currencies.mark = "$target_mark$");

DROP TABLE IF EXISTS DataMart1;
CREATE TABLE DataMart1
            (datetime datetime NOT NULL,
            base_currency_name varchar(50) NOT NULL,
            target_currency_name varchar(50) NOT NULL,
            rate float(40,20) NOT NULL);
INSERT INTO DataMart1 (
    DataMart1.datetime,
    DataMart1.base_currency_name,
    DataMart1.target_currency_name,
    DataMart1.rate) 
SELECT f1.datetime, f1.base_currency_name, f2.target_currency_name, f1.rate * f2.rate AS rate
FROM f1
LEFT JOIN f2
ON f1.datetime = f2.datetime;

DROP TABLE f1;
DROP TABLE f2;  
