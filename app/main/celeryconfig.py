from config import Config


broker_url = Config.BROKER_URL

include = ['app.main.tasks']

task_serializer = "json"
result_serializer = "json"
accept_content = ["json"]
timezone = Config.TIMEZONE
enable_utc = True

beat_schedule = {
    "fetch_currency_rates": {
        "task": "app.main.tasks.etl",
        "schedule": int(Config.ETL_TRANSFER_FREQUENCY)
    },
    "transfer_rates_from_staging_to_core": {
        "task": "app.main.tasks.update_core",
        "schedule": int(Config.CORE_TRANSFER_FREQUENCY)
    },
    "generate_datamarts_from_core": {
        "task": "app.main.tasks.generate_datamarts",
        "schedule": int(Config.DATAMARTS_GENERATION_FREQUENCY)
    }
}
