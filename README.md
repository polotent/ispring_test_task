# Data Warehouse for storing, managing and analysing currencies rates
## Description
A simple DWH (Data Warehouse), using MySQL and Python3. Python scripts are 3 celery tasks. They are executed periodically and manage 4 currencies  (EUR, USD, RUB, CNY) rates information.
## Requirements
* MySQL (or MariaDB)
* Python3
* pip for python3
* RabbitMQ (for Celery)
## Installation
### Pipenv
Install pipenv with:
```
pip install pipenv
```
### Project packages
cd to root directory of the project and install all needed packages with:
```
pipenv install
```
### Environment variables
You need to set up environment variables:
```
CURR_RATES_SOURCE_URL=source_url
CURR_RATES_SOURCE_ACCESS_KEY=access_key
CURR_BASE=EUR
CURR_LIST=USD,EUR,RUB,CNY
DWH_DB_HOST=localhost
DWH_DB_USER=your_db_user
DWH_DB_PASSWORD=your_db_password
ETL_TRANSFER_FREQUENCY=100
CORE_TRANSFER_FREQUENCY=200
DATAMARTS_GENERATION_FREQUENCY=30
BROKER_URL=pyamqp://guest@localhost//
TIMEZONE=Europe/Moscow
```
You can do it in a .env file. Frequencies are set in seconds.
## Usage
### DataMarts configuration
You can configure datamarts generation. Check out 'app/main/datamarts_config.json' config file. For each datamart there is a config dictionary, which contains properties for datamart generation. 

For 'datamart1' and 'datamart2' you can specify properties:
* 'target_mark' (target currency mark);
* 'language' (language display setting of currencies names).

For 'datamart3' you can set up properties:
* 'base_mark' (base currency mark);
* 'target_mark' (target currency mark);
* 'start_datetime' and 'end_datetime' (for specifying period of time). Currency rates will be shown only for this period.

Configuration example:
```
{
    "datamart1": {
        "target_mark": "RUB",
        "language": "russian"
    },
    "datamart2": {
        "target_mark": "USD",
        "language": "english"
    },
    "datamart3": {
        "base_mark": "RUB",
        "target_mark": "USD",
        "start_datetime": "2021-07-24",
        "end_datetime": "2021-07-31"
    }
}
```
### Launching
To run celery worker run in activated virtual environment:
```
pipenv shell
```
```
celery -A manage worker -l info
```
or just run:
```
pipenv run worker
```
To run celery beat run in activated virtual environment:
```
pipenv shell
```
```
celery -A manage beat -l info
```
or just run:
```
pipenv run beat
```
To see working process (tables), navigate to your database browser (e.g. phpmyadmin) and find 'dwh' db there.
