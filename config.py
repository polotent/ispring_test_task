from os import path, getenv
from dotenv import load_dotenv


# Load variables from .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))


# Basic config, which get info from environment variables.
class Config:
    CURR_RATES_SOURCE_URL = getenv("CURR_RATES_SOURCE_URL")
    CURR_RATES_SOURCE_ACCESS_KEY = getenv("CURR_RATES_SOURCE_ACCESS_KEY")
    CURR_BASE = getenv("CURR_BASE")
    CURR_LIST = getenv("CURR_LIST")
    DWH_DB_HOST = getenv("DWH_DB_HOST")
    DWH_DB_USER = getenv("DWH_DB_USER")
    DWH_DB_PASSWORD = getenv("DWH_DB_PASSWORD")
    ETL_TRANSFER_FREQUENCY = getenv("ETL_TRANSFER_FREQUENCY")
    CORE_TRANSFER_FREQUENCY = getenv("CORE_TRANSFER_FREQUENCY")
    DATAMARTS_GENERATION_FREQUENCY = getenv("DATAMARTS_GENERATION_FREQUENCY")
    BROKER_URL = getenv("BROKER_URL")
    TIMEZONE = getenv("TIMEZONE")
